"""
[IA02] TP SAT/Sudoku template python
author:  Sylvain Lagrue
version: 1.0.2
"""

from typing import List, Tuple
import subprocess
import itertools
from pprint import pprint

# alias de types
Variable = int
Literal = int
Clause = List[Literal]
Model = List[Literal]
Clause_Base = List[Clause]
Grid = List[List[int]]

example: Grid = [
    [5, 3, 0, 0, 7, 0, 0, 0, 0],
    [6, 0, 0, 1, 9, 5, 0, 0, 0],
    [0, 9, 8, 0, 0, 0, 0, 6, 0],
    [8, 0, 0, 0, 6, 0, 0, 0, 3],
    [4, 0, 0, 8, 0, 3, 0, 0, 1],
    [7, 0, 0, 0, 2, 0, 0, 0, 6],
    [0, 6, 0, 0, 0, 0, 2, 8, 0],
    [0, 0, 0, 4, 1, 9, 0, 0, 5],
    [0, 0, 0, 0, 8, 0, 0, 7, 9],
]


example2: Grid = [
    [0, 0, 0, 0, 2, 7, 5, 8, 0],
    [1, 0, 0, 0, 0, 0, 0, 4, 6],
    [0, 0, 0, 0, 0, 9, 0, 0, 0],
    [0, 0, 3, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 5, 0, 2, 0],
    [0, 0, 0, 8, 1, 0, 0, 0, 0],
    [4, 0, 6, 3, 0, 1, 0, 0, 9],
    [8, 0, 0, 0, 0, 0, 0, 0, 0],
    [7, 2, 0, 0, 0, 0, 3, 1, 0],
]


empty_grid: Grid = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
]

#### fonctions fournies


def write_dimacs_file(dimacs: str, filename: str):
    with open(filename, "w", newline="") as cnf:
        cnf.write(dimacs)


def exec_gophersat(
    filename: str, cmd: str = "gophersat", encoding: str = "utf8"
) -> Tuple[bool, List[int]]:
    result = subprocess.run(
        [cmd, filename], capture_output=True, check=True, encoding=encoding
    )
    string = str(result.stdout)
    lines = string.splitlines()

    if lines[1] != "s SATISFIABLE":
        return False, []

    model = lines[2][2:].split(" ")

    return True, [int(x) for x in model]

### fonction pour transformer une case en variable 
def cell_to_variable(i,j,val):
    v = 81*i + 9*j + val
    return v

### fonction pour transformer une varible en case
def variable_to_cell(v):
    i = (v-1)//81
    j = ((v-1)%81)//9
    c = ((v-1)%81)%9+1
    return (i,j,c)

### fonction at-least-one : au moins un vrai
def at_least_one(vars: List[int]) -> List[int]:
    return vars[:]

### fonction unique : une seule clause vraie et les autres fausses
def unique(vars: List[int]) -> List[List[int]]:
    cu = []
    cu.append(at_least_one(vars))
    for i in combinations(vars,2):
        i = [-x for x in i]
        cu.append(i)
    return cu

### fonction pour créer les contraintes d'unicité

def create_cell_constraints() -> List[List[int]]:
    list = []
    for i in range(9):
        for j in range(9):
            #contraintes possibles
            cp = [x for x in range(cell_to_variable(i,j,1),cell_to_variable(i,j,9)+1)]
            list.append(unique(cp))
    return list

### fonction pour créer les contraintes sur les lignes
def create_line_constraints() -> List[List[int]]:
    list = []
    for c in range(1,10): #to get all the colors
        for i in range(9):
            line = [] #to get constraints for every line
            for j in range(9): #to get every cell
                line.append(cell_to_variable(i,j,c))
            list.append(unique(line))
    return list


### fonction pour créer les contraintes sur les colonnes
def create_column_constraints() -> List[List[int]]: 
    list = []
    for c in range(1,10): #to get all the colors
        for j in range(9):
            column = [] #to get constraints for every column
            for i in range(9): #to get every cell
                column.append(cell_to_variable(i,j,c))
            list.append(unique(column))
    return list

### fonction pour créer les contraintes sur les carrées du sudoku
def create_box_constraints() -> List[List[int]]: 
    list = []

### fonction pour créer des contraintes unitaires déjà inscrites dans une grille
def create_value_constraints(grid: List[List[int]]) -> List[List[int]]: 

#### fonction principale


def main():
    pass


if __name__ == "__main__":
    main()